# frozen_string_literal: true

require "ekylibre/grpc/version"

require_relative 'grpc/pdf/converter_services_pb'

module Ekylibre
  module Grpc
  end
end
