# Generated by the protocol buffer compiler.  DO NOT EDIT!
# Source: ekylibre/grpc/pdf/converter.proto for package 'ekylibre.grpc.pdf'

require 'grpc'
require 'ekylibre/grpc/pdf/converter_pb'

module Ekylibre
  module Grpc
    module Pdf
      module OdtToPdf
        class Service

          include GRPC::GenericService

          self.marshal_class_method = :encode
          self.unmarshal_class_method = :decode
          self.service_name = 'ekylibre.grpc.pdf.OdtToPdf'

          rpc :convert_odt, Ekylibre::Grpc::Pdf::FileMessage, Ekylibre::Grpc::Pdf::FileMessage
        end

        Stub = Service.rpc_stub_class
      end
      module JasperPdf
        class Service

          include GRPC::GenericService

          self.marshal_class_method = :encode
          self.unmarshal_class_method = :decode
          self.service_name = 'ekylibre.grpc.pdf.JasperPdf'

          rpc :convert_jasper, Ekylibre::Grpc::Pdf::JasperMessage, Ekylibre::Grpc::Pdf::FileMessage
        end

        Stub = Service.rpc_stub_class
      end
    end
  end
end
