# frozen_string_literal: true

module Ekylibre
  module Grpc
    VERSION = "0.1.0"
  end
end
