# frozen_string_literal: true

require_relative 'lib/ekylibre/grpc/version'

Gem::Specification.new do |spec|
  spec.name = "ekylibre-grpc"
  spec.version = Ekylibre::Grpc::VERSION
  spec.authors = ["Ekylibre developers"]
  spec.email = ["dev@ekylibre.com"]

  spec.summary = %q{GRPC definitions for Ekylibre}
  spec.required_ruby_version = ">= 2.6.0"
  spec.homepage = "https://www.ekylibre.com"
  spec.license = "AGPL-3.0-only"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "https://gems.ekylibre.dev"

    spec.metadata["homepage_uri"] = spec.homepage
    spec.metadata["source_code_uri"] = "https://gitlab.com/ekylibre/microservices/ekylibre-grpc"
  else
    raise StandardError.new("RubyGems 2.0 or newer is required to protect against public gem pushes.")
  end

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.glob(%w[lib/**/*.rb bin/**/* *.gemspec Gemfile Rakefile *.md]) + Dir.glob('protos/**/*.proto')
  # spec.bindir        = "exe"
  # spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency 'grpc', '~> 1.30'
  spec.add_development_dependency 'grpc-tools', '~> 1.30'
end
