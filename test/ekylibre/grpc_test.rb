# frozen_string_literal: true

require "test_helper"

class Ekylibre::GrpcTest < Minitest::Test
  def test_that_it_has_a_version_number
    refute_nil ::Ekylibre::Grpc::VERSION
  end

  def pdf_constants_are_defined
    assert Ekylibre::Grpc::Pdf::OdtToPdf::Service
    assert Ekylibre::Grpc::Pdf::OdtToPdf::Stub
    assert Ekylibre::Grpc::Pdf::JasperPdf::Service
    assert Ekylibre::Grpc::Pdf::JasperPdf::Stub
  end
end
